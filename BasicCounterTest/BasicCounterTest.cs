﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterCS;

namespace BasicCounterTest
{
    [TestClass]
    public class BasicCounterTest
    {
        [TestMethod]
        public void IncrementTest()
        {
            BasicCounterClass.Increment();
            Assert.AreEqual(1,BasicCounterClass.getNumber());
        }
        [TestMethod]
        public void DecrementTest()
        {
            BasicCounterClass.Decrement();
            Assert.AreEqual(0, BasicCounterClass.getNumber());
        }
        [TestMethod]
        public void ZeroTest()
        {
            BasicCounterClass.Increment();
            BasicCounterClass.Increment();
            BasicCounterClass.Zero();
            Assert.AreEqual(0, BasicCounterClass.getNumber());
        }
    }
}
