﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterCS
{
    public class BasicCounterClass
    {
        private static int number=0;

        public BasicCounterClass()
        {
        }

        public static void Increment()
        {
            number++;
        }

        public static void Decrement()
        {
            number--;
        }

        public static void Zero()
        {
            number = 0;
        }

        public static int getNumber()
        {
            return number;
        }
    }
}
