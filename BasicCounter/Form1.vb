﻿Imports BasicCounterCS

Public Class BasicCounter
    Private Sub DecrementBtn_Click(sender As Object, e As EventArgs) Handles DecrementBtn.Click
        BasicCounterClass.Decrement()
        numberLabel.Text = String.Format("{0}", BasicCounterClass.getNumber())
    End Sub

    Private Sub IncrementBtn_Click(sender As Object, e As EventArgs) Handles IncrementBtn.Click
        BasicCounterClass.Increment()
        numberLabel.Text = String.Format("{0}", BasicCounterClass.getNumber())
    End Sub

    Private Sub ZeroBtn_Click(sender As Object, e As EventArgs) Handles ZeroBtn.Click
        BasicCounterClass.Zero()
        numberLabel.Text = String.Format("{0}", BasicCounterClass.getNumber())
    End Sub
End Class
