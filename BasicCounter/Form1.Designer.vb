﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DecrementBtn = New System.Windows.Forms.Button()
        Me.IncrementBtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.numberLabel = New System.Windows.Forms.Label()
        Me.ZeroBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'DecrementBtn
        '
        Me.DecrementBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.DecrementBtn.Location = New System.Drawing.Point(110, 154)
        Me.DecrementBtn.Name = "DecrementBtn"
        Me.DecrementBtn.Size = New System.Drawing.Size(203, 64)
        Me.DecrementBtn.TabIndex = 0
        Me.DecrementBtn.Text = "-"
        Me.DecrementBtn.UseVisualStyleBackColor = True
        '
        'IncrementBtn
        '
        Me.IncrementBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.IncrementBtn.Location = New System.Drawing.Point(540, 154)
        Me.IncrementBtn.Name = "IncrementBtn"
        Me.IncrementBtn.Size = New System.Drawing.Size(191, 64)
        Me.IncrementBtn.TabIndex = 1
        Me.IncrementBtn.Text = "+"
        Me.IncrementBtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(371, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 39)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Total"
        '
        'numberLabel
        '
        Me.numberLabel.AutoSize = True
        Me.numberLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.numberLabel.Location = New System.Drawing.Point(399, 167)
        Me.numberLabel.Name = "numberLabel"
        Me.numberLabel.Size = New System.Drawing.Size(36, 39)
        Me.numberLabel.TabIndex = 3
        Me.numberLabel.Text = "0"
        '
        'ZeroBtn
        '
        Me.ZeroBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.ZeroBtn.Location = New System.Drawing.Point(312, 267)
        Me.ZeroBtn.Name = "ZeroBtn"
        Me.ZeroBtn.Size = New System.Drawing.Size(224, 79)
        Me.ZeroBtn.TabIndex = 4
        Me.ZeroBtn.Text = "Set to Zero"
        Me.ZeroBtn.UseVisualStyleBackColor = True
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.ZeroBtn)
        Me.Controls.Add(Me.numberLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.IncrementBtn)
        Me.Controls.Add(Me.DecrementBtn)
        Me.Name = "BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DecrementBtn As Button
    Friend WithEvents IncrementBtn As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents numberLabel As Label
    Friend WithEvents ZeroBtn As Button
End Class
